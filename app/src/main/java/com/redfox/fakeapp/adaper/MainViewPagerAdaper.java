package com.redfox.fakeapp.adaper;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.redfox.fakeapp.fragment.DemoFragment;
import com.redfox.fakeapp.fragment.HomeFragment;

/**
 * Created by redfox on 2017/3/17.
 */

public class MainViewPagerAdaper extends FragmentStatePagerAdapter {

    public MainViewPagerAdaper(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0)
            return  new HomeFragment();
        else
            return new DemoFragment();
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "首页";
            case 1:
                return "热点";
            case 2:
                return "直播";
            case 3:
                return "我的";
        }
        return "Tab " + position;
    }
}
