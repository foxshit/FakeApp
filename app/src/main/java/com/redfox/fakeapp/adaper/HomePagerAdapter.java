package com.redfox.fakeapp.adaper;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.redfox.fakeapp.fragment.DemoFragment;
import com.redfox.fakeapp.fragment.RecommFragment;

/**
 * Created by redfox on 2017/3/17.
 */

public class HomePagerAdapter extends FragmentStatePagerAdapter {

    public HomePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new RecommFragment();

            case 3:
                return new RecommFragment();
        }
        return new DemoFragment();
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "精选";
            case 1:
                return "综艺";
            case 2:
                return "电视剧";
            case 3:
                return "电影";
            case 4:
                return "动漫";
        }
        return "Tab " + position;
    }
}
