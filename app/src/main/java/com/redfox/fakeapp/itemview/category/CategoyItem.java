package com.redfox.fakeapp.itemview.category;

import java.util.ArrayList;

/**
 * Created by redfox on 2017/3/18.
 */

public class CategoyItem {

    public static class CategoySubItem {
        public int ResId;
    }

    public String title;
    public ArrayList<CategoySubItem> mItems;
    public int mItemHeight = 80;

    public CategoyItem() {
        mItems = new ArrayList<>();
    }

    public void addItem(CategoySubItem item) {
        mItems.add(item);
    }

    public int size() {
        return mItems.size();
    }

    public CategoySubItem getItem(int index) {
        return mItems.get(index);
    }

}
