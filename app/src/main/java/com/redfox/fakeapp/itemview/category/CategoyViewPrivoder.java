package com.redfox.fakeapp.itemview.category;

import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.redfox.fakeapp.MyApp;
import com.redfox.fakeapp.R;

import me.drakeet.multitype.ItemViewProvider;

/**
 * Created by redfox on 2017/3/18.
 */

public class CategoyViewPrivoder extends ItemViewProvider<CategoyItem, CategoyViewHolder> {
    @NonNull
    @Override
    protected CategoyViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View v = inflater.inflate(R.layout.cate_recomm_layout, parent, false);
        return new CategoyViewHolder(v);
    }

    @Override
    protected void onBindViewHolder(@NonNull CategoyViewHolder holder, @NonNull CategoyItem categoyItem) {
        holder.title.setText(categoyItem.title);

        holder.grid.setLayoutManager(new GridLayoutManager(MyApp.getInst().getApplicationContext(), 2));
        holder.grid.setAdapter(new CategoyItemAdapter(categoyItem.mItems));
    }
}
