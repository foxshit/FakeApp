package com.redfox.fakeapp.itemview;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.redfox.fakeapp.R;

/**
 * Created by redfox on 2017/3/17.
 */

public class DemoViewHolder extends RecyclerView.ViewHolder {
    public TextView mText;
    public DemoViewHolder(View itemView) {
        super(itemView);
        mText = (TextView) itemView.findViewById(R.id.test_text);
    }
}
