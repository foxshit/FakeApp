package com.redfox.fakeapp.itemview.category;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.redfox.fakeapp.MyApp;
import com.redfox.fakeapp.R;

import java.util.ArrayList;

/**
 * Created by redfox on 2017/3/18.
 */

public class CategoyItemAdapter extends RecyclerView.Adapter<CategoyItemViewHolder> {
    public ArrayList<CategoyItem.CategoySubItem> mItems;

    public CategoyItemAdapter(ArrayList<CategoyItem.CategoySubItem> items) {
        super();
        mItems = items;
    }

    @Override
    public CategoyItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.categoy_item_layout, parent, false);
        return new CategoyItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CategoyItemViewHolder holder, int position) {
        holder.mImage.setImageDrawable(MyApp.getImage(mItems.get(position).ResId));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }
}
