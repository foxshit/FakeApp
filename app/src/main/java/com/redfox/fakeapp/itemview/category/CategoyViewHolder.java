package com.redfox.fakeapp.itemview.category;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.redfox.fakeapp.R;

/**
 * Created by redfox on 2017/3/18.
 */

public class CategoyViewHolder extends RecyclerView.ViewHolder {

    public TextView title;
    public RecyclerView grid;

    public CategoyViewHolder(View itemView) {
        super(itemView);

        title = (TextView) itemView.findViewById(R.id.cate_title);
        grid = (RecyclerView) itemView.findViewById(R.id.recycle_grid);
    }
}
