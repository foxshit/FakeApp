package com.redfox.fakeapp.itemview.banner;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.redfox.fakeapp.R;

import me.drakeet.multitype.ItemViewProvider;

/**
 * Created by redfox on 2017/3/17.
 */

public class BannerViewProvider extends ItemViewProvider<BannerViewItem, BannerViewHolder> {

    @NonNull
    @Override
    protected BannerViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View v = inflater.inflate(R.layout.banner_layout, parent, false);
        return new BannerViewHolder(v);
    }

    @Override
    protected void onBindViewHolder(@NonNull BannerViewHolder holder, @NonNull BannerViewItem bannerViewItem) {
        holder.mViewPager.setAdapter(new BannerAdapter(bannerViewItem));
    }
}
