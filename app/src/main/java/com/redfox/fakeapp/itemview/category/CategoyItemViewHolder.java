package com.redfox.fakeapp.itemview.category;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.redfox.fakeapp.R;

/**
 * Created by redfox on 2017/3/18.
 */

public class CategoyItemViewHolder extends RecyclerView.ViewHolder {
    public ImageView mImage;
    public CategoyItemViewHolder(View itemView) {
        super(itemView);
        mImage = (ImageView) itemView.findViewById(R.id.cate_image);
    }
}
