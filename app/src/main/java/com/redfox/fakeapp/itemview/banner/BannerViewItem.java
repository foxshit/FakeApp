package com.redfox.fakeapp.itemview.banner;

import java.util.ArrayList;

/**
 * Created by redfox on 2017/3/17.
 */

public class BannerViewItem {
    private ArrayList<BannerItem> mItems;

    public BannerViewItem() {
        mItems = new ArrayList<>();
    }

    public void add(BannerItem item) {
        mItems.add(item);
    }

    public int size() {
        return mItems.size();
    }

    public BannerItem get(int position) {
        return mItems.get(position);
    }
}
