package com.redfox.fakeapp.itemview;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.redfox.fakeapp.R;

import me.drakeet.multitype.ItemViewProvider;

/**
 * Created by redfox on 2017/3/17.
 */

public class DemoViewProvider extends ItemViewProvider<DemoItem, DemoViewHolder> {
    @NonNull
    @Override
    protected DemoViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View v = inflater.inflate(R.layout.demo_item_layout, parent, false);
        return new DemoViewHolder(v);
    }

    @Override
    protected void onBindViewHolder(@NonNull DemoViewHolder holder, @NonNull DemoItem demoItem) {
        holder.mText.setText(demoItem.str);
    }
}
