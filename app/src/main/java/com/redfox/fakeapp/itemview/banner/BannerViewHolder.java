package com.redfox.fakeapp.itemview.banner;

import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.redfox.fakeapp.R;

/**
 * Created by redfox on 2017/3/17.
 */

public class BannerViewHolder extends RecyclerView.ViewHolder {

    public ViewPager mViewPager;

    public BannerViewHolder(View itemView) {
        super(itemView);

        mViewPager = (ViewPager) itemView.findViewById(R.id.banner_pager);
    }
}
