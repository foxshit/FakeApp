package com.redfox.fakeapp.itemview.banner;

import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.redfox.fakeapp.MyApp;
import com.redfox.fakeapp.R;

/**
 * Created by redfox on 2017/3/17.
 */

public class BannerAdapter extends PagerAdapter {
    private BannerViewItem mData;
    public BannerAdapter(BannerViewItem data) {
        super();
        mData = data;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(container.getContext());
        View v = inflater.inflate(R.layout.banner_item_layout, container, false);
        ImageView image = (ImageView) v.findViewById(R.id.banner_image);
        image.setImageDrawable(MyApp.getImage(mData.get(position).bitmap_resid));
        container.addView(v);
        return v;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        //super.destroyItem(container, position, object);
        container.removeView((View)object);
    }
}
