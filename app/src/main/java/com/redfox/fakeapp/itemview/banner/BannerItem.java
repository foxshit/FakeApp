package com.redfox.fakeapp.itemview.banner;

/**
 * Created by redfox on 2017/3/17.
 */

public class BannerItem {
    public int bitmap_resid;
    public String bitmap_res_name;
    public String title;
    public String tagert_uri;
    public int width;
    public int height;

    public BannerItem() {
        super();
    }

    public BannerItem(int ResId) {
        super();
        bitmap_resid = ResId;
    }
}
