package com.redfox.fakeapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.redfox.fakeapp.R;
import com.redfox.fakeapp.itemview.DemoItem;
import com.redfox.fakeapp.itemview.DemoViewProvider;
import com.redfox.fakeapp.itemview.banner.BannerItem;
import com.redfox.fakeapp.itemview.banner.BannerViewItem;
import com.redfox.fakeapp.itemview.banner.BannerViewProvider;
import com.redfox.fakeapp.itemview.category.CategoyItem;
import com.redfox.fakeapp.itemview.category.CategoyViewPrivoder;

import me.drakeet.multitype.Items;
import me.drakeet.multitype.MultiTypeAdapter;

/**
 * Created by redfox on 2017/3/17.
 */

public class RecommFragment extends Fragment {
    private final String TAG = "RecommFragment";

    private RecyclerView mRecommView;
    private MultiTypeAdapter mAdapter;
    private Items mItems;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.recomm_layout, container, false);
        mRecommView = (RecyclerView) v.findViewById(R.id.recomm_view);

        mItems = new Items();
        mAdapter = new MultiTypeAdapter(mItems);

        mRecommView.setLayoutManager(new LinearLayoutManager(container.getContext(),
                LinearLayoutManager.VERTICAL, false));

        // register class
        mAdapter.register(BannerViewItem.class, new BannerViewProvider());
        mAdapter.register(DemoItem.class, new DemoViewProvider());
        mAdapter.register(CategoyItem.class, new CategoyViewPrivoder());

        // banner
        BannerViewItem banner = new BannerViewItem();
        BannerItem subItem = new BannerItem();
        subItem.bitmap_res_name = "";
        subItem.bitmap_resid = R.drawable.banner1;
        subItem.height = 80;
        subItem.width = 100;
        subItem.title = "this is test";

        banner.add(subItem);

        banner.add(new BannerItem(R.drawable.banner2));
        banner.add(new BannerItem(R.drawable.banner3));
        mItems.add(banner);

        // categoy_recommand
        CategoyItem categoyItem = new CategoyItem();
        categoyItem.title = "分类一";

        CategoyItem.CategoySubItem subItem1 = new CategoyItem.CategoySubItem();
        subItem1.ResId = R.drawable.cate1;
        categoyItem.addItem(subItem1);
        subItem1 = new CategoyItem.CategoySubItem();
        subItem1.ResId = R.drawable.cate2;
        categoyItem.addItem(subItem1);
        subItem1 = new CategoyItem.CategoySubItem();
        subItem1.ResId = R.drawable.cate3;
        categoyItem.addItem(subItem1);
        subItem1 = new CategoyItem.CategoySubItem();
        subItem1.ResId = R.drawable.cate4;
        categoyItem.addItem(subItem1);
        subItem1 = new CategoyItem.CategoySubItem();
        subItem1.ResId = R.drawable.cate5;
        categoyItem.addItem(subItem1);
        subItem1 = new CategoyItem.CategoySubItem();
        subItem1.ResId = R.drawable.cate6;
        categoyItem.addItem(subItem1);

        mItems.add(categoyItem);

        categoyItem = new CategoyItem();
        categoyItem.title = "其它分类一";

        subItem1 = new CategoyItem.CategoySubItem();
        subItem1.ResId = R.drawable.xcate1;
        categoyItem.addItem(subItem1);
        subItem1 = new CategoyItem.CategoySubItem();
        subItem1.ResId = R.drawable.xcate2;
        categoyItem.addItem(subItem1);
        subItem1 = new CategoyItem.CategoySubItem();
        subItem1.ResId = R.drawable.xcate3;
        categoyItem.addItem(subItem1);
        subItem1 = new CategoyItem.CategoySubItem();
        subItem1.ResId = R.drawable.xcate4;
        categoyItem.addItem(subItem1);

        mItems.add(categoyItem);


        categoyItem = new CategoyItem();
        categoyItem.title = "分类二";

        subItem1 = new CategoyItem.CategoySubItem();
        subItem1.ResId = R.drawable.cate1;
        categoyItem.addItem(subItem1);
        subItem1 = new CategoyItem.CategoySubItem();
        subItem1.ResId = R.drawable.cate2;
        categoyItem.addItem(subItem1);
        subItem1 = new CategoyItem.CategoySubItem();
        subItem1.ResId = R.drawable.cate3;
        categoyItem.addItem(subItem1);
        subItem1 = new CategoyItem.CategoySubItem();
        subItem1.ResId = R.drawable.cate4;
        categoyItem.addItem(subItem1);
        subItem1 = new CategoyItem.CategoySubItem();
        subItem1.ResId = R.drawable.cate5;
        categoyItem.addItem(subItem1);
        subItem1 = new CategoyItem.CategoySubItem();
        subItem1.ResId = R.drawable.cate6;
        categoyItem.addItem(subItem1);

        mItems.add(categoyItem);


        categoyItem = new CategoyItem();
        categoyItem.title = "分类三";

        subItem1 = new CategoyItem.CategoySubItem();
        subItem1.ResId = R.drawable.cate1;
        categoyItem.addItem(subItem1);
        subItem1 = new CategoyItem.CategoySubItem();
        subItem1.ResId = R.drawable.cate2;
        categoyItem.addItem(subItem1);
        subItem1 = new CategoyItem.CategoySubItem();
        subItem1.ResId = R.drawable.cate3;
        categoyItem.addItem(subItem1);
        subItem1 = new CategoyItem.CategoySubItem();
        subItem1.ResId = R.drawable.cate4;
        categoyItem.addItem(subItem1);
        subItem1 = new CategoyItem.CategoySubItem();
        subItem1.ResId = R.drawable.cate5;
        categoyItem.addItem(subItem1);
        subItem1 = new CategoyItem.CategoySubItem();
        subItem1.ResId = R.drawable.cate6;
        categoyItem.addItem(subItem1);

        mItems.add(categoyItem);

        // Demo
        DemoItem ditem = new DemoItem();
        ditem.str = "this is demo item";

        mItems.add(ditem);

        ditem = new DemoItem();
        ditem.str = "this is demo item";

        mItems.add(ditem);


        mRecommView.setAdapter(mAdapter);


        return v;
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState: ");
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated: ");
    }
}
