package com.redfox.fakeapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.redfox.fakeapp.R;
import com.redfox.fakeapp.adaper.HomePagerAdapter;

/**
 * Created by redfox on 2017/3/17.
 */

public class HomeFragment extends Fragment {
    private TabLayout mHomeTab;
    private ViewPager mHomeViewPager;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.home_frag_layout, container, false);
        mHomeTab = (TabLayout) v.findViewById(R.id.home_tab);
        mHomeViewPager= (ViewPager) v.findViewById(R.id.home_view_pager);

        mHomeViewPager.setAdapter(new HomePagerAdapter(getChildFragmentManager()));
        mHomeTab.setupWithViewPager(mHomeViewPager);

        return v;
    }
}
