package com.redfox.fakeapp.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.redfox.fakeapp.R;
import com.redfox.fakeapp.adaper.MainViewPagerAdaper;

public class MainActivity extends AppCompatActivity {
    private final String TAG = "MainActivity";

    private TabLayout mMainTab;
    private ViewPager mMainViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();

        mMainTab = (TabLayout) findViewById(R.id.main_tab);
        mMainViewPager = (ViewPager) findViewById(R.id.main_view_page);

        mMainViewPager.setAdapter(new MainViewPagerAdaper(getSupportFragmentManager()));

        mMainTab.setupWithViewPager(mMainViewPager);
        Log.d(TAG, "onCreate: ");

//        for (int i =0; i < mMainTab.getTabCount(); ++i)
//            mMainTab.getTabAt(i).setIcon(R.drawable.star);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState: ");
    }
}
