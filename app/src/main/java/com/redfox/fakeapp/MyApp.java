package com.redfox.fakeapp;

import android.app.Application;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

/**
 * Created by redfox on 2017/3/17.
 */

public class MyApp extends Application {
    private static MyApp _application = null;

    @Override
    public void onCreate() {
        super.onCreate();
        _application = this;
    }

    public static MyApp getInst() {
        return _application;
    }


    public static Drawable getImage(int ResId) {
        return ContextCompat.getDrawable(_application.getApplicationContext(), ResId);
    }

}
